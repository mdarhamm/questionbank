import Login from './components/Login';
import UserRegistration from './components/UserRegistration'
import { Route , Switch } from 'react-router-dom'
import EditQuestion from './components/EditQuestion'
import AllQuestion from './components/AllQuestion'
import AddQuestion from './components/AddQuestion'
import { BrowserRouter as Router } from 'react-router-dom';
import Layout from './components/Layout';

function App() {
  return (
    <Router>
      <Switch>
      <Route path="/login" exact>
        <Login />
        </Route>
      <Route path="/userregistration">
        <UserRegistration />
      </Route>
      
      <Layout>
        <Route path="/questionmaster">
          <AllQuestion />
        </Route>
        <Route path="/editquestion">
          <EditQuestion />
        </Route>
        <Route path="/addquestion">
          <AddQuestion />
        </Route>
      </Layout>
      </Switch>
    </Router>
  )
}

export default App;
