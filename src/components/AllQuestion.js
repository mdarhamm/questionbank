import { Button, Table, TableBody, TableCell , TableHead, TableRow } from '@material-ui/core'
import React from 'react'
import { Grid , Container } from '@material-ui/core'
import { DeleteTwoTone, EditTwoTone, ExpandMoreOutlined } from '@material-ui/icons'
import { useHistory } from "react-router"
import { useState } from 'react'
import { Dialog , DialogActions , DialogContent , DialogContentText } from '@material-ui/core'


export default function AllQuestion(){
    
    const history = useHistory()

    const [openDelete , setOpenDelete ] = useState(false)

    const handleClickOpen = () =>{
        setOpenDelete(true)
    }

    const handleClickClose = () => {
        setOpenDelete(false)
    }
    
    return(
        <div>
            {/**code for filter by status */}

            {/**Container to display icons for 
             * status , edit , delete and expand menu
             */}
            <Container>
            <Grid container>
            <Button>Status</Button>
            <Button
                 onClick={()=>history.push('./EditQuestion')}
                 startIcon={<EditTwoTone />}
                 />
            <Button 
                onClick={handleClickOpen}
                startIcon={<DeleteTwoTone />}
                />
                
            {/**open dialog for deletion confirmation */}
            <Dialog
                open={openDelete}
                onClose={handleClickClose}>
                    <DialogContent>
                        <DialogContentText >
                            Are you sure you wanna delete this ?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClickClose}>YES</Button>
                        <Button onClick={handleClickClose} autoFocus>
                            NO
                        </Button>
                    </DialogActions>
            </Dialog>
            {/**delete dialog closes */}
            <Button startIcon={<ExpandMoreOutlined />} />
            </Grid>
            </Container>
            {/**Table to display content for all questions */}
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Question</TableCell>
                        
                    </TableRow>
                </TableHead>
                <TableBody>

                </TableBody>
            </Table>

            {/**pagination */}
            {/*<Container spacing={2}>
                <Pagination count={10} />
      
            </Container>*/}
            </div>
    )
}