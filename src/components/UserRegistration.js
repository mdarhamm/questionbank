/**
 * Author : Arham
 * Description : UserRegistration() is used to create Registration page 
 * Props : 
 */
 import React from 'react'
 import { Grid , Paper , Avatar , TextField , Button, Typography } from "@material-ui/core"
 import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
 import { Link } from 'react-router-dom'
 
 function UserRegistration(){
 
     const paperStyle = {padding:30 , height:'70vh' , width:320 , margin:"40px auto"}
     const avatarStyle = {backgroundColor:'#99bbff'}
     const btnStyle = {margin:'12px 0'}
 
     return(
     <Grid>
         <Paper elevation={10} style={paperStyle}>        
             <Grid align='center'>
             <Avatar style={avatarStyle}><LockOutlinedIcon /></Avatar>
             <h2>USER REGISTRATION</h2>
             </Grid>
             <TextField 
                fullwidth="true" 
                label="Name" 
                placeholder="enter name" 
                required 
            />
             <TextField 
                fullwidth="true" 
                label="Username" 
                placeholder="enter username" 
                required />
             <TextField 
                fullwidth="true" 
                label="Email" 
                placeholder="enter email" 
                type='email' 
                required />
             <br/>
             <Button 
                variant="contained" 
                style={btnStyle} 
                type="" 
                color='primary' 
                fullwidth="true">
                Cancel
             </Button>
             <br/>
             <Button 
                variant="contained" 
                style={btnStyle} 
                type="submit" 
                color='primary' 
                fullwidth="true"
             >Register
             </Button>
             
             <Typography>
                 Already a Member ? Sign In from here
             
             </Typography>
             
             <Link to="/login">Login</Link>
         </Paper>
     </Grid>
     )
 
 }
 
 export default UserRegistration
 
     