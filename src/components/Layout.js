import React from "react"
import { useState } from "react"
import { makeStyles, Toolbar } from "@material-ui/core"
import { Button } from "@material-ui/core"
import { Drawer } from '@material-ui/core'
import { AccountCircleOutlined, MenuOutlined} from "@material-ui/icons"
import { List , ListItem , ListItemText , ListItemIcon } from "@material-ui/core"
import { AddCircleOutlined, SubjectOutlined } from "@material-ui/icons"
import { AppBar } from "@material-ui/core"

//list for drawer items 
const menuItems = [
    {
        text : 'My Profile' ,
        icon : <AccountCircleOutlined />,
        path : ' '
    },
    {
        text : 'Hola !' ,
        icon : <AddCircleOutlined />,
        path : ' '
    },
    {
        text : 'Settings' ,
        icon : <SubjectOutlined />,
        path : ' '
    }
]

const useStyles = makeStyles((theme)=>{
    return {
    drawer :{
        width : 250
    },
    appbar : {
        backgroundColor : '#f9f9f0'
    },
   //to mix content of appbar and the content the chidren has 
    toolbar: theme.mixins.toolbar,
    page :{
        backgroundColor:'#f9f9f0',
        width:'100%'
    },
    root :{
        display: 'flex'
    }
}})

export default function Layout({children}){
    
    const classes = useStyles()
    const [isDrawerOpen,setDrawerOpen] = useState(false)

    const toggleDrawer = (open) => (event) => {
        setDrawerOpen(open)
      }

    return (
        <div className={classes.root}>

            {/**app bar */}
            <AppBar
             className={classes.appbar}
             elevation={0}
             >
                <Toolbar className={classes.toolbar}>
                    <Button 
                        startIcon={<MenuOutlined />}
                        onClick={toggleDrawer(true)} 
                    />

                    <Button>Logout</Button>
                </Toolbar>
            </AppBar>

            {/**drawer */}
            
                <Drawer
                    className = {classes.drawer}
                    anchor = {'left'}
                    open = {isDrawerOpen}
                    onClose = {toggleDrawer(false)}
                >
                <List>
                    {menuItems.map(item => (
                        <ListItem
                            button
                            key={item.text}>
                                <ListItemIcon>{item.icon}</ListItemIcon>
                                <ListItemText primary={item.text} />
                        
                        </ListItem>
                    ))}
                </List>
                </Drawer>

        <div className={classes.page}>
           {/**div to mix the content of app bar and children */}
            <div className={classes.toolbar}></div>
                {children}
            
        </div>

        </div>
        
    )
}