/**
 * Author : Arham
 * Description : Login() is used to create login page 
 * Props : 
 */
 import React from 'react'
 import { Grid , Paper , Avatar , TextField , Button, Typography } from "@material-ui/core"
 import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
 import { Link } from 'react-router-dom'
 import { useState } from 'react'


 function Login(){

    const[ setUser , isSetUser ] = useState()
    const [ setPassword , isSetPassword] = useState()

    const loginHandle = (e) => {
        e.preventDefault()

        if(setUser && setPassword)
        {
            console.log(setUser,setPassword)
        }
    }

    
     const paperStyle = {padding:30 , height:'70vh' , width:320 , margin:"40px auto"}
     const avatarStyle = {backgroundColor:'#99bbff'}
     const btnStyle = {margin:'20px 0'}
 
     return(
     <Grid>
         <Paper elevation={10} style={paperStyle}>        
             <Grid align='center'>
             <Avatar style={avatarStyle}><LockOutlinedIcon /></Avatar>
             <h2>LOGIN</h2>
             </Grid>
             <form
                onSubmit={loginHandle}
             >
             <TextField 
                onChange = {(e) => {isSetUser(e.target.value)}}
                fullwidth="true" 
                label="Username" 
                placeholder="enter username" 
                required />
             <TextField 
                onChange = {(e) => {isSetPassword(e.target.value)}}
                fullwidth="true" 
                label="Password" 
                placeholder="enter password" 
                type='password' 
                required />
             <br/>
             <Button
                onSubmit={loginHandle} 
                variant="contained" 
                style={btnStyle} 
                type="submit" 
                color='primary' 
                fullwidth="true">
                Login
            </Button>
            </form>
             <Typography>
                <Link to="#" fullwidth="true">Forgot Password ?</Link>
             </Typography>
 
             <Typography>
                 New Member? Click Here To Register !
                 <br/>
             <Link to="/userregistration">New User Registration</Link>
             </Typography>
         </Paper>
     </Grid>
     )
 }
 
 export default Login