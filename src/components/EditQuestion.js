import React from 'react'
import { Button, Container } from '@material-ui/core'
import { TextField } from '@material-ui/core'
import { makeStyles } from '@material-ui/core'
import { useState } from 'react'

const useStyles = makeStyles({
  field:{
    marginTop: 50
  },
  btn:{
    marginTop: 20,
    marginBottom: 20
  }
})

export default function EditQuestion() {

  const classes = useStyles()

  const [editQuestion , isEditQuestion] = useState()

  const handleSubmit = (e) => {

    e.preventDefault()

    if(editQuestion != null)
    console.log(editQuestion)

  }

  return (
        <Container>
          <form 
          onSubmit = {handleSubmit}
          className={classes.field}>
          <TextField 
            onChange={ (e) => { isEditQuestion(e.target.value) }}
            id="outlined-basic" 
            label="Edit Question" 
            variant="outlined"
            multiline
            rows={5}
            fullWidth
            required
            />

            <Button 
              className={classes.btn}
              variant="contained" 
              color="secondary"
              type="submit"
              >Answer
              </Button>
            <br/>
            {/*<Button variant="contained" color="textSecondary">Update</Button>*/}
          </form>
        </Container>
    )
}