import React from 'react'
import { Button, Container } from '@material-ui/core'
import { TextField } from '@material-ui/core'
import { makeStyles } from '@material-ui/core'
import { useState } from 'react'

const useStyles = makeStyles({
    field:{
        marginTop: 20,
        marginBottom: 20,
        display: 'block'
      },
  btn:{
    marginTop: 20,
    marginBottom: 20
  }
})

export default function AddQuestion() {

  const classes = useStyles()

  const [setQuestion , isSetQuestion] = useState()
  const [setAnswer , isSetAnswer] = useState()

    const handleSubmit = (e) => {
        e.preventDefault()

        if(setQuestion && setAnswer )
        {
            console.log(setQuestion,setAnswer)
        }
    }

  return (
        <Container>
          <form onSubmit={handleSubmit}>
          <TextField
            className={classes.field}
            onChange={ (e) => {isSetQuestion(e.target.value)}} 
            id="outlined-basic" 
            label="Question" 
            variant="outlined"
            multiline
            rows={4}
            fullWidth
            required
            />

        <TextField 
            className={classes.field}
            onChange = {(e) => { isSetAnswer(e.target.value) }}
            id="outlined-basic-2" 
            label="Answer" 
            variant="outlined"
            multiline
            rows={8}
            fullWidth
            required
            />

            <Button 
                className={classes.btn}
                variant="contained" 
                color="secondary"
                type="submit"
                >
                SAVE
            </Button>
          </form>
        </Container>
    )
}